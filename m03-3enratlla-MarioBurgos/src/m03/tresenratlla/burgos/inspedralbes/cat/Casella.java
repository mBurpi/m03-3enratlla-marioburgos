package m03.tresenratlla.burgos.inspedralbes.cat;

/*
 * La classe Casella gestiona els tokens per torn (a cada torn posara X o O)
 */
public class Casella {

	// atributs
	String token = "";
	private final String TK1 = "O";  //TK abreviacio de TOKEN
	private final String TK2 = "X";
	private boolean ocupada = false;
	int jugadaPer = 0;

	// constructor
	public Casella(boolean ocupada) {
		this.ocupada = ocupada; 
	}

	// getters i setters
	public boolean getOcupada() {
		return this.ocupada;
	}
	
	public void setOcupada(boolean ocupada) {
		this.ocupada = ocupada;
	}
	
	public String getToken() {
		return this.token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	
	public int getJugadaPer() {
		return this.jugadaPer;
	}
	
	public void setJugadaPer(int jugadaPer) {
		this.jugadaPer = jugadaPer;
	}
	
	

	// metodes:

	public String toString() {
		String string = "";
		if (!ocupada) {
			string = "___|";
		}else if (jugadaPer % 2 == 0) {
			this.setToken(TK1);
			string = "_" + getToken() + "_|";
		} else {
			this.setToken(TK2);
			string = "_" + getToken() + "_|";
		}
		return string;
	}

}