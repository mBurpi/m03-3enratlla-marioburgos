package m03.tresenratlla.burgos.inspedralbes.cat;

import java.util.Scanner;

public class Huma extends Jugador{
	
	Scanner scan = new Scanner(System.in);

	//Atribut heredat de la classe Jugador
	
	//constructor de jugador Huma
	public Huma(String nom) {
		super(nom);
	}

	//metodes
	
	@Override
	public int[] indicarJugada() {
		int[] jugada = new int[2];
		System.out.print("\n" + nom + ": Indica fila: \n ");
		jugada[0] = scan.nextInt();
		System.out.print("\n" + nom + ": Indica columna: \n ");
		jugada[1] = scan.nextInt();
		return jugada;
	}

}
