package m03.tresenratlla.burgos.inspedralbes.cat;

import java.util.Scanner;

public class Partida {

	// Atributs
	int[] jugada = new int[2];
	Jugador[] participants = new Jugador[2];
	Casella[][] caselles = new Casella[3][3];
	Tauler tauler = new Tauler(caselles);
	int[] resultats = new int[3];

	Scanner scan = new Scanner(System.in);

	// constructor de Partides

	// metodes
	// menu de joc
	public void mostrarMenu() {
		int opcio = -1;
		do {
			System.out.println("--------MENU PRINCIPAL--------\n");
			tauler.netejarTauler();
			System.out.println(tauler.toString());
			System.out.println("\n Selecciona una opcio i prem enter\n");
			System.out.println("1. PLAYER VS PLAYER");
			System.out.println("2. PLAYER VS BOT");
			System.out.println("3. BOT VS BOT");
			System.out.println("0. EXIT");
			opcio = scan.nextInt();
			System.out.println("------------------------------");

			switch (opcio) {
			case 1:
				// nova partida
				resetMarcador(resultats);
				participants[0] = new Huma("Jugador 1");
				participants[1] = new Huma("Jugador 2");
				jugarPartida(participants);
				mostraResultats();
				break;

			case 2:
				resetMarcador(resultats);
				participants[0] = new Huma("Jugador 1");
				participants[1] = new Maquina("IA Rand 2");
				jugarPartida(participants);
				mostraResultats();
				break;

			case 3:
				resetMarcador(resultats);
				participants[0] = new Maquina("IA Rand 1");
				participants[1] = new Maquina("IA Rand 2");
				int n = 0;
				System.out.println("Indica quantes partides jugaran:");
				n = scan.nextInt();

				for (int i = 0; i < n; i++) {
					tauler.netejarTauler();
					jugarPartida(participants);
				}
				mostraResultats();
				break;

			default:
				break;
			}

		} while (opcio != 0);

	}

	public int[] resetMarcador(int[] marcador) {
		for (int i = 0; i < marcador.length; i++) {
			marcador[i] = 0;
		}

		return marcador;
	}

	private void mostraResultats() {
		System.out.println("Victories P1: " + resultats[0]);
		System.out.println("Victories P2: " + resultats[1]);
		System.out.println("Empats: " + resultats[2]);
	}

	// metode que inicia la partida i pren les jugades
	public int[] jugarPartida(Jugador[] participants) {
		tauler.netejarTauler();
		for (int i = 0; i < 9; i++) {
			do {
				if (tauler.torn % 2 == 0) {
					jugada = participants[0].indicarJugada();
				} else {
					jugada = participants[1].indicarJugada();
				}	
			} while (!tauler.jugarCasella(jugada));
			tauler.jugarCasella(jugada);
//			tauler.torn++;
//			tauler.comprovarGuanyador();
			if (tauler.comprovarGuanyador() == "O") {
				System.out.println("\n\n\nGuanyador: " + participants[0].nom + "\n\n\n");
				resultats[0]++;
			} else if (tauler.comprovarGuanyador() == "X") {
				System.out.println("\n\n\nGuanyador: " + participants[1].nom + "\n\n\n");
				resultats[1]++;
			} else if (i < 9) {
//					System.out.println("El joc continua...");
			} else {
				System.out.println("EMPAT! ");
				resultats[2]++;
			}
		}
		return resultats;
	}

}
