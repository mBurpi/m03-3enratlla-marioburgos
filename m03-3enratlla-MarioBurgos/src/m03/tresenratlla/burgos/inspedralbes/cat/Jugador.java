package m03.tresenratlla.burgos.inspedralbes.cat;

/*
 * La classe Jugador nomes  guarda el nom
 * i te un metode per indicar les jugades.
 * Cada classe que hereda de Jugador, implementa el seu propi metode indicarJugada()
 */
public abstract class Jugador {
	
	//atributs
	protected String nom;
	protected int[]jugada = new int[2];
	
	//constructor
	public Jugador (String nom) {
		this.setNom(nom);
	}

	//setter i getter
	public String getNom() {
		return this.nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}

	//metodes
	public abstract int[]indicarJugada();
	

}
