package m03.tresenratlla.burgos.inspedralbes.cat;

/*
 * la classe Tauler gestiona les jugades, mira si les caselles estan ocupades
 * i comprova el guanyador
 */

public class Tauler {

	// atributs
	int torn = 0;
	Casella[][] caselles = new Casella[3][3];
	int jugada[] = new int[2];
 
	// constructor
	public Tauler(Casella[][] caselles) {
		for (int i = 0; i < caselles.length; i++) {
			for (int j = 0; j < caselles.length; j++) {
				this.caselles[i][j] = new Casella(false); //el constructor rep l'atribut <ocupada>
			}
		}
	}

	// metodes

	// metode per netejar el tauler
	public void netejarTauler() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				caselles[i][j].setOcupada(false);
				torn = 0;
				caselles[i][j].setJugadaPer(-1);
			}
		}
	}

	/// Metode toString que imprimeix l'estat actual del tauler
	// nomes pinta les vores superior i esquerra y crida al metode
	/// toString() de Casella.
	@Override
	public String toString() {
		// borde superior
		String string = "          0   1   2\n         ___ ___ ___\n";
		for (int i = 0; i < caselles.length; i++) {
			// borde izquierdo
			string += "      " + i + " |";
			for (int j = 0; j < caselles.length; j++) {
				string += caselles[i][j].toString();
			}
			string += "\n";
		}
		return string;
	}

	// metode que efectua les jugades.
	// Rep les coordenades c1 i c2, i modifica el valor caselles[c1][c2].ocupada =
	// true
	// i asigna el torn a l'atribut jugadaPer de Casella. Si es parell la juga O, si
	// no X.
	public boolean jugarCasella(int jugada[]) {
		
		//System.out.println(jugada[0] + "," + jugada[1]);
		
		// comprovar la legalitat de la jugada
		if (jugada[0] >= 0 && jugada[0] < 3) {
			if (jugada[1] >= 0 && jugada[1] < 3) {
				if (!caselles[jugada[0]][jugada[1]].getOcupada() && caselles[jugada[0]][jugada[1]].jugadaPer <= 0) {
					caselles[jugada[0]][jugada[1]].setOcupada(true);
					caselles[jugada[0]][jugada[1]].setJugadaPer(torn);
					System.out.println("\n=============================");
					System.out.println("  Aixi ha anat la jugada: " + (torn + 1) + "\n");
					System.out.println(toString() + "\n=============================\n");
					torn++;
					return true;
				} else {
					System.out.println("La Casella (" + jugada[0] + "," + jugada[1] + ") esta  ocupada!");
					return false;
				}
			} else {
				System.out.println("La columna indicada esta  fora dels marges del tauler!!");
				return false;
			}
		} else {
			System.out.println("La fila indicada esta  fora dels marges del tauler!!");
			return false;
		}

	}

	// metode que comprova el guanyador
	public String comprovarGuanyador() {

		String string = "";

		if (torn >= 5) {
			// comprova files
			for (int i = 0; i < 3; i++) {
				if (caselles[i][0].getOcupada()&&caselles[i][1].getOcupada() && caselles[i][2].getOcupada()) {
					if (caselles[i][0].getToken().equals(caselles[i][1].getToken())) {
						if (caselles[i][1].getToken().contentEquals(caselles[i][2].getToken())) {
							string = caselles[i][0].getToken();
							return string;
//							if (i == 0) {
//								System.out.println("primera fila");
//							}
//							if (i == 1) {
//								System.out.println("segona fila");
//							}
//							if (i == 2) {
//								System.out.println("tercera fila");
//							}
						}
					}
				}
				if (caselles[0][i].getOcupada()&&caselles[1][i].getOcupada() && caselles[2][i].getOcupada()) {
					if (caselles[0][i].getToken().equals(caselles[1][i].getToken())) {
						if (caselles[1][i].getToken().contentEquals(caselles[2][i].getToken())) {
							string = caselles[0][i].getToken();
							return string;
//							if (i == 0) {
//								System.out.println("primera col");
//							}
//							if (i == 1) {
//								System.out.println("segona col");
//							}
//							if (i == 2) {
//								System.out.println("tercera col");
//							}
						}
					}
					
					//diagonals
					if (caselles[0][0].getToken().equals(caselles[1][1].getToken())) {
						if (caselles[1][1].getToken().equals(caselles[2][2].getToken())) {
							string = caselles[0][0].getToken();
							return string;
//							System.out.println("diagonal");

						}
					}
					if (caselles[0][2].getToken().equals(caselles[1][1].getToken())) {
						if (caselles[1][1].getToken().equals(caselles[2][0].getToken())) {
							string = caselles[0][2].getToken();
							return string;
//							System.out.println("diagonal");

						}
					}
				}
			}
		}
		return string;
	}
}